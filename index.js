console.log('hi');

// Without the use objects, our students from before would be organized as follows if we are to record additinal information about them
//create student one
// let studentOneName = 'Tony';
// let studentOneEmail = 'starksindustries@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Peter';
// let studentTwoEmail = 'spideyman@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Wanda';
// let studentThreeEmail = 'scarlettMaximoff@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Steve';
// let studentFourEmail = 'captainRogers@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

// Encapsualte the following information into 4 student objects using object literals
let studentOne = {
    name: "Tony",
    email: "starksindustries@mail.com",
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        this.grades.forEach(grade => console.log(`${this.name}'s quarterly grade averages are: ${grade}`));
    },

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },

    // Mini Activity
    willPass() {
        return this.computeAve() >= 85;
    },

    willPassWithHonors() {
        return this.willPass() && this.computeAve() >= 90;
    }
};

console.log(`Student one's name is: ${studentOne.name}`)
console.log(studentOne.listGrades());
console.log(studentOne.login());
console.log(studentOne.computeAve());
console.log(studentOne.willPass());
console.log(studentOne.willPassWithHonors());

console.log(this); // result: global window object


let studentTwo = {
    name: "Peter",
    email: "spideyman@mail.com",
    grades: [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        this.grades.forEach(grade => console.log(`${this.name}'s quarterly grade averages are: ${grade}`));
    },

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },

    // Mini Activity
    willPass() {
        return this.computeAve() >= 85;
    },

    willPassWithHonors() {
        return this.willPass() && this.computeAve() >= 90;
    }
};

let studentThree = {
    name: "Wanda",
    email: "scarlettMaximoff@mail.com",
    grades: [87, 89, 91, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        this.grades.forEach(grade => console.log(`${this.name}'s quarterly grade averages are: ${grade}`));
    },

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },

    // Mini Activity
    willPass() {
        return this.computeAve() >= 85;
    },

    willPassWithHonors() {
        return this.willPass() && this.computeAve() >= 90;
    }
};

let studentFour = {
    name: "Steve",
    email: "captainRogers@mail.com",
    grades: [91, 89, 92, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },

    logout() {
        console.log(`${this.email} has logged out`);
    },

    listGrades() {
        this.grades.forEach(grade => console.log(`${this.name}'s quarterly grade averages are: ${grade}`));
    },

    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum += grade);
        return sum / this.grades.length;
    },

    // Mini Activity
    willPass() {
        return this.computeAve() >= 85;
    },

    willPassWithHonors() {
        return this.willPass() && this.computeAve() >= 90;
    }
};

const classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents() {
        return this.students.filter(student => student.willPassWithHonors()).length;
    },

    honorsPercentage() {
        return (this.countHonorStudents() / this.students.length) * 100;
    },

    retrieveHonorStudentInfo() {
        return this.students.filter(student => student.willPassWithHonors())
                .map(honorStudents => {
                    return {
                        email: honorStudents.email,
                        aveGrade: honorStudents.computeAve()
                    }
                });
    },

    sortHonorStudentsByGradeDesc() {
        return this.retrieveHonorStudentInfo().sort((a, b) => {
            return a.aveGrade - b.aveGrade;
        }).reverse();
    }
}

console.log(classOf1A.countHonorStudents());
console.log(classOf1A.honorsPercentage());
console.log(classOf1A.retrieveHonorStudentInfo());
console.log(classOf1A.sortHonorStudentsByGradeDesc());










// Mini-Quiz:
/*
    1. What is the term given to unorganized code that's very hard to work with?
        - Spaghetti Code

    2. How are object literals written in JS?
        - key-value pairs inside {}

    3. What do you call the concept of organizing information and functionality to belong to an object?
        - Encapsulation

    4. If studentOne has a method named enroll(), how would you invoke it?
        - studentOne.enroll();

    5. True or False: Objects can have objects as properties.
        - true

    6. What does the this keyword refer to if used in an arrow function method?
        - refers to the properties inside the local scope of an object where the function also belongs

    7. True or False: A method can have no parameters and still work.
        - true

    8. True or False: Arrays can have objects as elements.
        - true

    9. True or False: Arrays are objects.
        - true

    10. True or False: Objects can have arrays as properties.
        - true

*/


// function Student(name, email, grades) {
//     this.name = name;
//     this.email = email;
//     this.grades = grades;

//     // methods
//     this.login = () => console.log(`${email} has logged in`) ;
//     this.logout = () => console.log(`${email} has logged in`) ;
//     this.listGrades = () => grades.forEach(grade => console.log(grade));
// };

// const student1 = new Student('Charles Patrick Lilagan', 'charles.lilagan@gmail.com', [91,90,89, 94]);
// console.log(student1);
// console.log(student1.getName());



